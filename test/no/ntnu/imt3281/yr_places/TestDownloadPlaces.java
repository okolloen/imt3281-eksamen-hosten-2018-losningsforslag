package no.ntnu.imt3281.yr_places;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

/**
 * Tests that we are able to download data from "http://fil.nrk.no/yr/viktigestader/noreg.txt" and decode it correctly.
 * The data should be returned as a list containing a list of strings for each line in the source file. The lists within
 * the list should then contain one String for each field on the corresponding line in the file (the lines contains tab 
 * delimited fields.
 *  
 * @author oeivindk
 *
 */
public class TestDownloadPlaces {

	@Test
	public void testDownloadFunction() {
		List<List<String>> places = DownloadPlaces.downloadPlaces();	// Static method, returning a list containing lists of Strings
		int numPlaces = places.size();									// Was 10996 as of oct. 11. 2018
		assertTrue (numPlaces>10500);
		
		// First line contains field description, used to check for correct decoding of the file
		List<String> firstLine = places.get(0);
		assertEquals("Kommunenummer", firstLine.get(0));
		assertEquals("Stadnamn", firstLine.get(1));
		assertEquals("Prioritet", firstLine.get(2));
		assertEquals("Stadtype nynorsk", firstLine.get(3));
		assertEquals("Stadtype bokmål", firstLine.get(4));
		assertEquals("Stadtype engelsk", firstLine.get(5));
		assertEquals("Kommune", firstLine.get(6));
		assertEquals("Fylke", firstLine.get(7));
		assertEquals("Lat", firstLine.get(8));
		assertEquals("Lon", firstLine.get(9));
		assertEquals("Høgd", firstLine.get(10));
		assertEquals("Nynorsk", firstLine.get(11));
		assertEquals("Bokmål", firstLine.get(12));
		assertEquals("Engelsk", firstLine.get(13));
	}
}
