package no.ntnu.imt3281.yr_places;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestDownloadPlaces.class, TestPlace.class, TestStore.class })
public class AllTests {

}
