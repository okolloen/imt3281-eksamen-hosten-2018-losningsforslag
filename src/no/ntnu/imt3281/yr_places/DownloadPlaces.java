package no.ntnu.imt3281.yr_places;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DownloadPlaces {
	public static List<List<String>>downloadPlaces() {
		List<List<String>> places = new ArrayList<>();
		URL u=null;
		try {
			u = new URL("http://fil.nrk.no/yr/viktigestader/noreg.txt");
		} catch (MalformedURLException e) {
			System.exit(0);
		}
		try (BufferedReader br = new BufferedReader(new InputStreamReader(u.openStream()))) {
			String tmp=null;
			while ((tmp=br.readLine())!=null) {
				String data[] = tmp.split("\t");
				places.add(Arrays.asList(data));
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}		
		return places;
	}
}
