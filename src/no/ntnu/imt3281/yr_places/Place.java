package no.ntnu.imt3281.yr_places;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Place {
	int kommunenr;
	String stedsnavn;
	String stedstype;
	String kommune;
	String fylke;
	double lat;
	double lng;
	String url;
	
	public Place (List<String> data) {
		kommunenr = Integer.parseInt(data.get(0));
		stedsnavn = data.get(1);
		stedstype = data.get(4);
		kommune = data.get(6);
		fylke = data.get(7);
		lat = Double.parseDouble(data.get(8));
		lng = Double.parseDouble(data.get(9));
		if (!data.get(12).equals("http://www.yr.no/sted/Norge/")) {
			url = data.get(12);
		} else {
			url = data.get(11);
		}
	}

	public Place(ResultSet res) throws SQLException {
		kommunenr = res.getInt(1);
		stedsnavn = res.getString(2);
		stedstype = res.getString(3);
		kommune = res.getString(4);
		fylke = res.getString(5);
		lat = res.getDouble(6);
		lng = res.getDouble(7);
		url = res.getString(8);
	}

	public int getKommunenr() {
		return kommunenr;
	}

	public String getStedsnavn() {
		return stedsnavn;
	}

	public String getStedstype() {
		return stedstype;
	}

	public String getKommune() {
		return kommune;
	}

	public String getFylke() {
		return fylke;
	}

	public double getLat() {
		return lat;
	}

	public double getLng() {
		return lng;
	}

	public String getVarselURL() {
		return url;
	}
	
	public String toString () {
		return String.format("%f:%f > %s i %s, %s", lat, lng, stedsnavn, kommune, fylke);
	}

}
