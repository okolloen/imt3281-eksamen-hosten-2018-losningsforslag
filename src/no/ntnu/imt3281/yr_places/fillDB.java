package no.ntnu.imt3281.yr_places;

import java.sql.SQLException;
import java.util.List;

public class fillDB {
	public static void main(String[] args) throws SQLException {
		DataStore store = DataStore.getDataStore();
		List<List<String>> places = DownloadPlaces.downloadPlaces();
		places.remove(0);	// Remove line with headings
		places.forEach(place->{
			store.addPlace(new Place(place));
		});
		System.out.println(store.placesInStore());
	}
}
