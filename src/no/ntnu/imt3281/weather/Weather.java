package no.ntnu.imt3281.weather;

import java.sql.SQLException;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import no.ntnu.imt3281.yr_places.DataStore;
import no.ntnu.imt3281.yr_places.Place;
import no.ntnu.imt3281.xml.XMLHandler;

public class Weather {
    @FXML private WebView map;
    @FXML private WebView forecast;
    @FXML private Label harMannenFalt;

    private DataStore places = null;
    
    
    public Weather () {
    	try {
			places = DataStore.getDataStore();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
    }
    
    @FXML
    public void initialize() {
    	WebEngine mapEngine = map.getEngine();
    	mapEngine.load("http://localhost/map/index.html");
    	WebEngine forecastHTML = forecast.getEngine();
    	mapEngine.setOnAlert(e->{
    		String[] pos = e.getData().split("\t");
    		double lat = Double.parseDouble(pos[0]);
    		double lng = Double.parseDouble(pos[1]);
    		try {
				Place place = places.getClosestPlace(lat, lng);
				XMLHandler reader = new XMLHandler(place.getVarselURL());
				System.out.println(place.getVarselURL());
				StringBuffer sb = new StringBuffer();
				sb.append(String.format("<h2>%s i %s</h2><br><br>%n", place.getStedsnavn(), place.getKommune()));
				reader.getForecast().forEach(line->{
					sb.append(line);
					sb.append("<br>");
					sb.append("\n");
				});
				forecastHTML.loadContent(sb.toString());
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
    	});
    	((BorderPane)forecast.getParent()).bottomProperty().set(new MannenLabel());
    }
}