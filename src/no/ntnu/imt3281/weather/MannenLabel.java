package no.ntnu.imt3281.weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javafx.scene.control.Label;

public class MannenLabel extends Label {
	public MannenLabel () {
		try {
			URL u = new URL("https://www.harmannenfalt.no/");
			BufferedReader br = new BufferedReader(new InputStreamReader(u.openStream()));
			String tmp=null;
			while ((tmp=br.readLine())!=null) {
				if (tmp.trim().equals("<div id=\"yesnomaybe\">")) {
					tmp = br.readLine();
					if (tmp.trim().equals("NEI")) {
						setText("Har mannen falt: Nei");
					} else {
						setText("OIOIOI, mannen har ramla ned!");
					}
				}
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
