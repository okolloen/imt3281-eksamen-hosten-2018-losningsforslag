package no.ntnu.imt3281.click_areas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Polygon;
import javafx.stage.FileChooser;

public class CreateClickAreas {

    @FXML private ListView<Point> points;
    @FXML private TextField areaName;
    @FXML private TextArea areas;
    @FXML private AnchorPane imageContainer;
    
    ObservableList<Point> pointList = FXCollections.observableArrayList();
    Polygon polygon = new Polygon();
    List<String> arrays = new ArrayList<>();
    
    @FXML
    public void initialize() {
    	points.setItems(pointList);
    	imageContainer.getChildren().add(polygon);
    	polygon.setStyle("-fx-stroke: rgba(0, 0, 0, 1); -fx-fill: rgba(100%, 100%, 100%, 0.5);");
    	areas.setText("[]");
    }

    @FXML
    void addArea(ActionEvent event) {
    	StringBuffer sb = new StringBuffer();
    	sb.append(String.format("{%n\t\"name\": \"%s\",%n", areaName.getText()));
    	sb.append("\t\"polygon\": [\n");
    	pointList.forEach(point->{
    		sb.append(String.format(new Locale("en"), "\t\t{\"x\": %.1f, \"y\": %.1f},%n", point.getX(), point.getY()));
    	});
    	sb.deleteCharAt(sb.length()-2);
    	sb.append("\t]\n}");
    	arrays.add(sb.toString());
    	areas.setText(arrays.toString());
        // Doing this using a JSON object also gives a full score
        
    	pointList.clear();
    	polygon.getPoints().clear();
    }

    @FXML
    void findPos(MouseEvent event) {
    	pointList.add(new Point(event.getSceneX(), event.getSceneY()));
    	polygon.getPoints().add(event.getSceneX()-10);
    	polygon.getPoints().add(event.getSceneY()-10);
    }

    @FXML
    void toFile(ActionEvent event) {
    	FileChooser fileChooser = new FileChooser();
    	fileChooser.setTitle("Velg fil");
    	
    	fileChooser.setInitialDirectory(new File("."));
    	File f = fileChooser.showSaveDialog(imageContainer.getParent().getScene().getWindow());
    	
    	if (f!=null) {
    		try (BufferedWriter bw = new BufferedWriter(new FileWriter(f))) {
				bw.write(areas.getText());
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    }
}