package no.ntnu.imt3281.click_areas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class ClickAreas extends Application {

    @FXML private Label areaToClick;
    @FXML private TextArea results;
    
    HashMap<String, Polygon> shapes = new HashMap<>();
    Iterator<String> shapeNames;

    @FXML
    public void initialize() {
    	getShapesFromFile();
    	shapeNames = shapes.keySet().iterator();
    	areaToClick.setText(shapeNames.next());
    }

	private void getShapesFromFile() {
		try (BufferedReader br = new BufferedReader(new FileReader("former.json"))) {
			String tmp;
			StringBuffer sb = new StringBuffer();
			while ((tmp=br.readLine())!=null) {
				sb.append(tmp);
			}
			json2shapes(sb.toString());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void json2shapes(String jsonData) {
		JSONArray json = new JSONArray(jsonData);
		json.forEach(object->{															// Bruk av forEach belønnes
			JSONArray polygon = (JSONArray) ((JSONObject)object).get("polygon");
			List<Point> points = new ArrayList<>();
			polygon.forEach(point->{
				points.add(new Point(((JSONObject)point).getDouble("x"), ((JSONObject)point).getDouble("y")));
			});
			shapes.put(((JSONObject)object).get("name").toString(), 
					new Polygon(points.toArray(new Point[points.size()])));
		});
	}
    
    @FXML
    void clicked(MouseEvent event) {
    	if (shapes.get(areaToClick.getText()).contains(new Point(event.getSceneX(), event.getSceneY()))) {
    		results.setText(String.format("%sDet er helt riktig.%n", results.getText()));
    		if (shapeNames.hasNext()) {
    			areaToClick.setText(shapeNames.next());
    		} else {
    			results.setText(String.format("%s%nDu har klart alle formene.", results.getText()));
    		}
    	} else {
    		results.setText(String.format("%sDet var dessverre feil, du skal klikke på %s.%n", results.getText(), areaToClick.getText()));
    	}
    }
    
    @Override
	public void start(Stage primaryStage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("ClickAreas.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Verktøy for å definere områder");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
